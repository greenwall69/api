// Dependency

import express, { Application, Request, Response } from 'express'

import Mongoose from './config/mongoose'
import articleRoutes from './routes/article.route'
import authRoutes from './routes/authRoutes'
import authV2Route from './routes/auth.v2.route'
import bodyParser from 'body-parser'
import compression from 'compression'
import cors from 'cors'
import { config as dotenv } from 'dotenv'
import helmet from 'helmet'
import morgan from 'morgan'
import participantRoutes from './routes/participant.route'
import propertyRoutes from './routes/property.route'
import userRoutes from './routes/userRoutes'

// Routes

class App {
    public app: Application

    constructor() {
        this.app = express()
        this.plugins()
        this.routes()
    }

    public plugins(): void {
        this.app.use(bodyParser.json())
        this.app.use(bodyParser.urlencoded({ extended: true }))
        this.app.use(morgan('dev'))
        this.app.use(compression())
        this.app.use(helmet())
        this.app.use(cors())
        dotenv()
    }

    public routes(): void {
        this.app.route('/api/hello').get((req: Request, res: Response) => {
            res.send('Hello World!')
        })
        // this.app.use(express.static(__dirname + 'public'))
        this.app.use('/api/v1/auth', authRoutes)
        this.app.use('/api/v2/auth', authV2Route)
        this.app.use('/api/v1/users', userRoutes)
        this.app.use('/api/v1/properties', propertyRoutes)
        this.app.use('/api/v1/participants', participantRoutes)
        this.app.use('/api/v1/articles', articleRoutes)
    }

    public start(): void {
        const port: number = 3001
        this.app.listen(port, () => {
            console.log(`Server is listenning on port ${port}`)
        })
    }
}

const mongoose = new Mongoose()
const app = new App()
app.start()
