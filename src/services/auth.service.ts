import { Request, Response } from 'express'
import User, { IUser } from '../models/user.model'

import Authentication from '../utils/authentication'

class AuthenticationServices {
    protocol: Request['protocol']
    params: Request['params']
    body: Request['body']
    host: Request['hostname']
    file: Request['file']
    auth: { id: string }

    constructor(req: Request) {
        this.auth = req.app.locals.user
        this.body = req.body
        this.params = req.params
        this.file = req.file
        this.protocol = req.protocol
        this.host = req.hostname
    }

    signup = async () => {
        let { username, email, password, role } = this.body

        const hashedPassword: string = await Authentication.passwordHash(
            password,
        )

        const user = new User({
            username,
            email,
            password: hashedPassword,
            role: role || 'member',
        })

        let token = Authentication.generateToken(
            user.id,
            user.username,
            user.password,
        )

        await user.save()

        return token
    }

    findUser = async () => {
        let { email } = this.body
        let user = await User.findOne({ email })

        return user
    }

    updateProfile = async () => {
        const { id } = this.auth
        // const { email, username, name, birthday, about } = this.body
        // const { avatar, identityCard, selfieWithIdentityCard } = this.body.image
        const user = await User.updateOne(
            { _id: id },
            this.body,
            // {
            //     $set: {
            //         this.body
            // email,
            // username,
            // name,
            // birthday,
            // about,
            // 'image.avatar': avatar,
            // 'image.identityCard': identityCard,
            // 'image.selfieWithIdentityCard': selfieWithIdentityCard,
            //     },
            // },
        )
        return user
    }
}

export default AuthenticationServices
