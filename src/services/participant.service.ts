import Participant, { IParticipant } from '../models/participants.model'
import Property, { IProperty } from '../models/property.model'
import { Request, Response } from 'express'

class ParticipantServices {
    protocol: Request['protocol']
    params: Request['params']
    body: Request['body']
    host: Request['hostname']
    file: Request['file']
    auth: { id: string }

    constructor(req: Request) {
        this.auth = req.app.locals.user
        this.body = req.body
        this.params = req.params
        this.file = req.file
        this.protocol = req.protocol
        this.host = req.hostname
    }

    findAll = async () => {
        const participant = Participant.find({})
            .populate({
                path: 'propertyId',
                select: 'title',
            })
            .populate({
                path: 'userId',
                select: 'username',
            })

        return participant
    }

    findbyId = async () => {
        const { id } = this.params
        const participant = await Participant.findById({ _id: id })
            .populate({
                path: 'propertyId',
            })
            .populate({
                path: 'userId',
            })

        return participant
    }

    findbyProperty = async () => {
        const { propertyId } = this.params
        const participant = Participant.find({ propertyId, status: 'verified' })
            .populate({
                path: 'propertyId',
                select: 'title',
            })
            .populate({
                path: 'userId',
                select: 'username',
            })

        return participant
    }

    findbyStatus = async () => {
        const status = this.params
        const participant = Participant.find({ status })
            .populate({
                path: 'propertyId',
                select: 'title',
            })
            .populate({
                path: 'userId',
                select: 'username',
            })

        return participant
    }

    store = async () => {
        const { propertyId, reason } = this.body
        const userId = this.auth
        const participant = new Participant({
            propertyId,
            userId,
            reason,
        })

        await participant.save()

        return participant
    }

    update = async () => {
        const { id } = this.params
        const { reason } = this.body
        const participant = await Participant.updateOne(
            { _id: id },
            {
                $set: {
                    reason,
                },
            },
        )

        return participant
    }

    delete = async () => {
        const { id } = this.params
        const participant = await Participant.deleteOne({ _id: id })

        return participant
    }

    verification = async () => {
        const { id } = this.params
        const { propertyId, userId, status } = this.body
        if (status == 'verified') {
            const participant = await Participant.updateOne(
                { _id: id },
                {
                    $set: {
                        status,
                    },
                },
            )

            const property = await Property.updateOne(
                { _id: propertyId },
                {
                    $addToSet: {
                        participants: { _id: userId },
                    },
                },
                { safe: true, upsert: true },
            )
            return participant
        } else if (status == 'rejected') {
            const participant = await Participant.updateOne(
                { _id: id },
                {
                    $set: {
                        status,
                    },
                },
            )

            const property = await Property.updateOne(
                { _id: propertyId },
                {
                    $pull: {
                        participants: { _id: userId },
                    },
                },
                { safe: true, upsert: true },
            )

            return participant
        }
    }
}

export default ParticipantServices
