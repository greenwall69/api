import Property, { IProperty } from '../models/property.model'
import { Request, Response } from 'express'

class PropertyServices {
    protocol: Request['protocol']
    params: Request['params']
    body: Request['body']
    host: Request['hostname']
    file: Request['file']
    auth: { id: string }

    constructor(req: Request) {
        this.auth = req.app.locals.user
        this.body = req.body
        this.params = req.params
        this.file = req.file
        this.protocol = req.protocol
        this.host = req.hostname
    }

    getAll = async () => {
        const property = await Property.find({})
            .populate('createdBy', 'username')
            .populate('verifiedBy', 'username')
            .populate({
                path: 'participants',
                select: 'username',
            })

        return property
    }

    store = async () => {
        const createdBy = this.auth
        const { title, image } = this.body
        const { text, area, dueDate } = this.body.description
        const {
            address,
            city,
            province,
            postal_code,
            latitude,
            longitude,
        } = this.body.location
        const property = new Property({
            createdBy,
            title,
            image,
            'description.text': text,
            'description.area': area,
            'description.dueDate': dueDate,
            'location.address': address,
            'location.city': city,
            'location.region': province,
            'location.postal_code': postal_code,
            'location.latitude': latitude,
            'location.longitude': longitude,
        })
        await property.save()
        return property
    }

    showById = async () => {
        const { id } = this.params
        const property = await Property.findById({ _id: id })
            .populate('createdBy', 'username')
            .populate('verifiedBy', 'username')

        return property
    }

    showByStatus = async () => {
        const { status } = this.body
        const property = await Property.find({ status: status })
            .populate('createdBy', 'username')
            .populate('verifiedBy', 'username')

        return property
    }

    update = async () => {
        const { id } = this.params
        const { title, image } = this.body
        const { text, location, area, dueDate } = this.body.description
        const {
            address,
            city,
            province,
            postal_code,
            latitude,
            longitude,
        } = this.body.location
        const findProperty = await Property.findOne({ _id: id })
        let status = findProperty?.status
        if (status == 'unverified') {
            const property = await Property.updateOne(
                { _id: id },
                {
                    $set: {
                        title: title,
                        image: image,
                        'description.text': text,
                        'description.area': area,
                        'description.dueDate': dueDate,
                        'location.address': address,
                        'location.city': city,
                        'location.region': province,
                        'location.postal_code': postal_code,
                        'location.latitude': latitude,
                        'location.longitude': longitude,
                    },
                },
            )

            return property
        }

        return "Campaign have to verify, don't able to updated data"
    }

    delete = async () => {
        const { id } = this.params
        const findProperty = await Property.findOne({ _id: id })
        let status = findProperty?.status
        if (status == 'unverified') {
            const property = await Property.findOneAndDelete({ _id: id })

            return property
        }

        return "Campaign have to verify, don't able to deleted data"
    }

    verification = async () => {
        const { id } = this.params
        const { status } = this.body
        const property = await Property.updateOne(
            { _id: id },
            {
                $set: {
                    status,
                    verifiedBy: this.auth,
                },
            },
        )

        return property
    }
}

export default PropertyServices
