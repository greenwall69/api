import Article, { IArticle } from '../models/article.model'
import { Request, Response } from 'express'

class ArticleServices {
    protocol: Request['protocol']
    params: Request['params']
    body: Request['body']
    host: Request['hostname']
    file: Request['file']
    auth: { id: string }

    constructor(req: Request) {
        this.auth = req.app.locals.user
        this.body = req.body
        this.params = req.params
        this.file = req.file
        this.protocol = req.protocol
        this.host = req.hostname
    }

    findAll = async () => {
        const article = Article.find({}).limit(5).populate('author', 'username')

        return article
    }

    store = async () => {
        const author = this.auth
        const { image, title, subtitle, content, status } = this.body
        const article = new Article({
            author,
            image,
            title,
            content,
            status,
        })

        await article.save()
        return article
    }

    show = async () => {
        const { id } = this.params
        const article = await Article.findById({ _id: id }).populate(
            'author',
            'username',
        )

        return article
    }

    update = async () => {
        const { id } = this.params
        const { image, title, content, status } = this.body
        const article = await Article.updateOne(
            { _id: id },
            {
                image,
                title,
                content,
                status,
            },
        )
        return article
    }

    delete = async () => {
        const { id } = this.params
        const article = await Article.deleteOne({ _id: id })

        return article
    }
}

export default ArticleServices
