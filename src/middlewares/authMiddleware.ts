import { Request, Response, NextFunction } from 'express'
import User, { IUser } from '../models/user.model'
import jwt from 'jsonwebtoken'

export const auth = async (
    req: Request,
    res: Response,
    next: NextFunction,
): Promise<any> => {
    if (!req.headers.authorization) {
        return res.status(401).send('Authorization failed!, No token available')
    }

    let secretKey = process.env.JWT_SECRET_KEY || 'secret'
    const token: string = req.headers.authorization.split(' ')[1]

    try {
        const credential: string | object = jwt.verify(token, secretKey)
        if (!credential) {
            return res
                .status(401)
                .send('Authorization Failed!, Token is Not Valid')
        }

        req.app.locals.credential = credential
        const x = req.app.locals.credential

        const user = await User.findOne({ username: x.username })
        if (!user) {
            throw new Error('User not found')
        }

        req.app.locals.user = user
        return next()
    } catch (error) {
        return res.send(error)
    }
}

export const authRoles = (role: string) => {
    return (req: Request, res: Response, next: NextFunction) => {
        try {
            const roles: string = req.app.locals.user.role
            if (roles !== role) {
                return res.status(401).json({
                    error:
                        "You don't have enough permission to perform this action",
                })
            }
            return next()
        } catch (error) {
            return res.send(error)
        }
    }
}

// export const grantAccess = async (action: any, resource: string, req: Request, res: Response, next: NextFunction): Promise<any> => {
//     try {
//         const services: Roles = new Roles()
//         const roles = await services.ManageRoles()
//         const role = req.app.locals.user.role
//         const permission = roles.can(role)[action](resource)
//         if (!permission.granted) {
//             return res.status(401).json({
//                 error: "You don't have enough permission to perform this action"
//             });
//         }
//         return next()
//     } catch (error) {
//         return res.send(error);
//     }

// }
