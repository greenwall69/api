import { validateSignin, validateSignup } from '../middlewares/authValidator'

import { auth } from '../middlewares/authMiddleware'
// Controllers
import authController from '../controllers/auth.controller'
import baseRoutes from './baseRoutes'

class authRoutes extends baseRoutes {
    public routes(): void {
        this.router.post('/signup', validateSignup, authController.signup)
        this.router.post('/signin', validateSignin, authController.signin)
        this.router.get('/profile', auth, authController.profile)
        this.router.put('/profile', auth, authController.updateProfile)
    }
}

export default new authRoutes().router
