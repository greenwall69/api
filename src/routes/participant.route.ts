import { auth, authRoles } from '../middlewares/authMiddleware'

import baseRoutes from './baseRoutes'
import participantController from '../controllers/participant.controller'

class ParticipantRoute extends baseRoutes {
    public routes(): void {
        // participants management
        this.router.get('/', auth, participantController.index)
        this.router.post('/', auth, participantController.store)
        this.router.get('/:id', auth, participantController.show)
        this.router.get(
            '/property/:propertyId',
            auth,
            participantController.showPropertyId,
        )
        this.router.put('/:id', auth, participantController.update)
        this.router.put(
            '/:id/verification',
            auth,
            participantController.verification,
        )
        this.router.delete('/:id', auth, participantController.delete)
    }
}

export default new ParticipantRoute().router
