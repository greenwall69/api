import { validateSignin, validateSignup } from '../middlewares/authValidator'

import { auth } from '../middlewares/authMiddleware'
import authController from '../controllers/auth.controller'
import baseRoutes from './baseRoutes'

// Controllers

class authRoutes extends baseRoutes {
    public routes(): void {
        this.router.post(
            '/signup',
            validateSignup,
            authController.signupWithUser,
        )
        this.router.post(
            '/signin',
            validateSignin,
            authController.signinWithUser,
        )
    }
}

export default new authRoutes().router
