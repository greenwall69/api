import { auth, authRoles } from '../middlewares/authMiddleware'

import articleController from '../controllers/article.controller'
import baseRoutes from './baseRoutes'

class ParticipantRoute extends baseRoutes {
    public routes(): void {
        // article management
        this.router.get('/', articleController.index)
        this.router.post('/', auth, articleController.store)
        this.router.get('/:id', articleController.show)
        this.router.put('/:id', auth, articleController.update)
        this.router.delete('/:id', auth, articleController.destroy)
    }
}
export default new ParticipantRoute().router
