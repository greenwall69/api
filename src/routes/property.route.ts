import { auth, authRoles } from '../middlewares/authMiddleware'

import PropertyController from '../controllers/property.controller'
import { Roles } from '../utils/role'
import baseRoutes from './baseRoutes'

class PropertyRoutes extends baseRoutes {
    public routes(): void {
        // property management
        this.router.get('/', auth, PropertyController.index)
        this.router.post('/', auth, PropertyController.store)
        this.router.post('/status', auth, PropertyController.showByStatus)
        this.router.get('/:id', auth, PropertyController.show)
        this.router.put('/:id', auth, PropertyController.update)
        this.router.delete('/:id', auth, PropertyController.destroy)
        this.router.put(
            '/:id/verification',
            auth,
            PropertyController.verification,
        )
    }
}

export default new PropertyRoutes().router
