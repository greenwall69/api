class Helper {
    public static compressedArray = async (
        originalArr: any[],
    ): Promise<any[]> => {
        let compressed: any[] = []
        let sortedArr = originalArr.sort()
        let copyArr: any[] = sortedArr.slice(0)

        for (let i = 0; i < sortedArr.length; i++) {
            let myCount: number = 0
            // loop over every element in the copy and see if it's the same
            for (let j = 0; j < copyArr.length; j++) {
                if (sortedArr[i] === copyArr[j]) {
                    // increase amount of times duplicate is found
                    myCount++
                    // sets item to undefined
                    delete copyArr[j]
                }
            }

            if (myCount > 0) {
                let result: any = new Object()
                result.count = myCount
                result.value = sortedArr[i]
                compressed.push(result)
            }
        }

        return compressed
    }
}

export default Helper
