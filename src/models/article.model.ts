import User, { IUser } from './user.model'
import mongoose, { Document, Schema } from 'mongoose'

export interface IArticle extends Document {
    author: IUser['_id']
    image: string[]
    title: string
    content: string
    status: 'draft' | 'published'
}

const articleSchema = new Schema(
    {
        author: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        image: [],
        title: {
            type: String,
            trim: true,
            required: true,
        },
        content: {
            type: String,
            trim: true,
            required: true,
        },
        status: {
            type: String,
            trim: true,
            default: 'draft',
        },
    },
    {
        timestamps: true,
    },
)

export default mongoose.model<IArticle>('Article', articleSchema)
