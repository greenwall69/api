import Property, { IProperty } from './property.model'
import User, { IUser } from './user.model'
import mongoose, { Document, Schema } from 'mongoose'

export interface IParticipant extends Document {
    propertyId: IProperty['_id']
    userId: IUser['_id']
    status: 'unverified' | 'verified' | 'rejected'
    reason: string
}

const participantSchema = new Schema(
    {
        propertyId: {
            type: Schema.Types.ObjectId,
            ref: 'Property',
        },
        userId: {
            type: Schema.Types.ObjectId,
            ref: 'User',
        },
        status: {
            type: String,
            trim: true,
            default: 'unverified',
        },
        reason: {
            type: String,
            trim: true,
            required: true,
        },
    },
    {
        timestamps: true,
    },
)

export default mongoose.model<IParticipant>('Participant', participantSchema)
