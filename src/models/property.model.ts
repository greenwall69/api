import User, { IUser } from './user.model'
import mongoose, { Document, Schema } from 'mongoose'

export interface IProperty extends Document {
    image: string
    title: string
    description: {
        text: string
        area: string
        dueDate: string
    }
    location: {
        address: string
        city: string
        province: string
        postal_code: string
        latitude: string
        longitude: string
    }
    createdBy: IUser['_id']
    verifiedBy: IUser['_id']
    status: 'unverified' | 'verified' | 'rejected' | 'completed'
    participants: [
        {
            _id: IUser['_id']
        },
    ]
}

const propertySchema = new Schema(
    {
        image: {
            type: String,
        },
        title: {
            type: String,
            required: true,
            trim: true,
        },
        description: {
            text: {
                type: String,
                required: true,
                trim: true,
            },
            area: {
                type: String,
                required: true,
            },
            dueDate: {
                type: String,
                required: true,
            },
        },
        location: {
            address: {
                type: String,
                trim: true,
            },
            city: {
                type: String,
                trim: true,
            },
            region: {
                type: String,
                trim: true,
            },
            postal_code: {
                type: String,
                trim: true,
            },
            country: {
                type: String,
                trim: true,
            },
            latitude: {
                type: String,
                trim: true,
            },
            longitude: {
                type: String,
                trim: true,
            },
        },
        createdBy: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
        verifiedBy: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            default: null,
        },
        status: {
            type: String,
            required: true,
            trim: true,
            default: 'unverified',
        },
        participants: [
            {
                _id: {
                    type: Schema.Types.ObjectId,
                    ref: 'User',
                    default: null,
                },
            },
        ],
    },
    {
        timestamps: true,
    },
)

export default mongoose.model<IProperty>('Property', propertySchema)
