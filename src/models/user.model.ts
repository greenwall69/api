import mongoose, { Document, Schema } from 'mongoose'

export interface IUser extends Document {
    email: string
    password: string
    username: string
    name: string
    about: string
    birthday: string
    phoneNumber: string
    image: {
        avatar: string
        identityCard: string
        selfieWithIdentityCard: string
    }
    role: 'member' | 'admin'
}

const userSchema = new Schema(
    {
        email: {
            type: String,
            required: true,
            unique: true,
            lowercase: true,
            trim: true,
        },
        password: {
            type: String,
            required: true,
            trim: true,
        },
        username: {
            type: String,
            required: true,
            unique: true,
            lowercase: true,
            trim: true,
        },
        name: {
            type: String,
            trim: true,
        },
        birthday: {
            type: String,
            trim: true,
        },
        about: {
            type: String,
            trim: true,
            default: 'Available',
        },
        number: {
            type: String,
            trim: true,
            default: null,
        },
        image: {
            avatar: {
                type: String,
                trim: true,
                default: null,
            },
            identityCard: {
                type: String,
                trim: true,
                default: null,
            },
            selfieWithIdentitiyCard: {
                type: String,
                trim: true,
                defautl: null,
            },
        },
        role: {
            type: String,
            default: 'member',
            enum: ['member', 'admin'],
        },
    },
    {
        timestamps: true,
    },
)

userSchema.methods.toJSON = function () {
    let obj = this.toObject()
    delete obj.password
    return obj
}

export default mongoose.model<IUser>('User', userSchema)
