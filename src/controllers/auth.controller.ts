import { NextFunction, Request, Response, Router } from 'express'
import User, { IUser } from '../models/user.model'

import Authentication from '../utils/authentication'
import AuthenticationServices from '../services/auth.service'

class authController {
    signup = async (req: Request, res: Response): Promise<Response> => {
        let { username, email, password, role } = req.body
        const hashedPassword: string = await Authentication.passwordHash(
            password,
        )
        const user = new User({
            username,
            email,
            password: hashedPassword,
            role: role || 'member',
        })

        let token = Authentication.generateToken(
            user.id,
            user.username,
            user.password,
        )
        await user.save()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'New user created',
            token,
        })
    }

    signupWithUser = async (req: Request, res: Response): Promise<Response> => {
        const services: AuthenticationServices = new AuthenticationServices(req)
        const token = await services.signup()
        const user = await services.findUser()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'New user created',
            token: token,
            user: user,
        })
    }

    signin = async (req: Request, res: Response): Promise<Response> => {
        // find user by email
        let { email, password } = req.body
        let user = await User.findOne({ email })
        if (!user) {
            return res.status(422).send({
                status: res.statusCode,
                success: false,
                messages: 'Email or Password Wrong',
            })
        }

        // check password
        let compare = await Authentication.passwordCompare(
            password,
            user.password,
        )
        if (!compare) {
            return res.status(422).send({
                status: res.statusCode,
                success: false,
                messages: 'Email or Password Wrong',
            })
        }

        // generate jwt-token
        let token = Authentication.generateToken(
            user.id,
            user.username,
            user.password,
        )

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Auth successfully',
            token,
        })
    }

    signinWithUser = async (req: Request, res: Response): Promise<Response> => {
        // find user by email
        let { email, password } = req.body
        let user = await User.findOne({ email })
        if (!user) {
            return res.status(422).send({
                status: res.statusCode,
                success: false,
                messages: 'Email or Password Wrong',
            })
        }

        // check password
        let compare = await Authentication.passwordCompare(
            password,
            user.password,
        )
        if (!compare) {
            return res.status(422).send({
                status: res.statusCode,
                success: false,
                messages: 'Email or Password Wrong',
            })
        }

        // generate jwt-token
        let token = Authentication.generateToken(
            user.id,
            user.username,
            user.password,
        )

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Auth successfully',
            token: token,
            user: user,
        })
    }

    profile = async (req: Request, res: Response): Promise<Response> => {
        return res.send(req.app.locals.user)
    }

    updateProfile = async (req: Request, res: Response): Promise<Response> => {
        const services: AuthenticationServices = new AuthenticationServices(req)
        const user = await services.updateProfile()
        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'User updated',
            user: user,
        })
    }
}

export default new authController()
