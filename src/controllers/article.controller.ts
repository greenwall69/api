import { Request, Response } from 'express'

import ArticleServices from '../services/article.service'
import IController from './interfaceControllers'

class ArticleController implements IController {
    index = async (req: Request, res: Response): Promise<Response> => {
        const services: ArticleServices = new ArticleServices(req)
        const article = await services.findAll()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Data loaded',
            article,
        })
    }

    store = async (req: Request, res: Response): Promise<Response> => {
        const services: ArticleServices = new ArticleServices(req)
        const article = await services.store()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Data loaded',
            article,
        })
    }

    show = async (req: Request, res: Response): Promise<Response> => {
        const services: ArticleServices = new ArticleServices(req)
        const article = await services.show()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Data loaded',
            article,
        })
    }

    update = async (req: Request, res: Response): Promise<Response> => {
        const services: ArticleServices = new ArticleServices(req)
        const article = await services.update()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Data updated',
        })
    }

    destroy = async (req: Request, res: Response): Promise<Response> => {
        const services: ArticleServices = new ArticleServices(req)
        const article = await services.delete()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Data deleted',
        })
    }
}

export default new ArticleController()
