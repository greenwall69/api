import { Request, Response } from 'express'
import User, { IUser } from '../models/user.model'

class userController {
    profile = async (req: Request, res: Response): Promise<Response> => {
        return res.send(req.app.locals.user)
    }
}

export default new userController()
