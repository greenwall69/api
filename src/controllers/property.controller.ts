import { Request, Response } from 'express'

import IController from './interfaceControllers'
import PropertyServices from '../services/property.service'

class PropertyController implements IController {
    index = async (req: Request, res: Response): Promise<Response> => {
        const services: PropertyServices = new PropertyServices(req)
        const property = await services.getAll()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Data has been loaded',
            property,
        })
    }

    store = async (req: Request, res: Response): Promise<Response> => {
        const services: PropertyServices = new PropertyServices(req)
        const property = await services.store()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'New data created',
            property,
        })
    }

    show = async (req: Request, res: Response): Promise<Response> => {
        const services: PropertyServices = new PropertyServices(req)
        const property = await services.showById()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Data has been loaded',
            property,
        })
    }

    showByStatus = async (req: Request, res: Response): Promise<Response> => {
        const services: PropertyServices = new PropertyServices(req)
        const property = await services.showByStatus()

        return res.send({
            status: res.statusCode,
            success: true,
            property,
        })
    }

    update = async (req: Request, res: Response): Promise<Response> => {
        const services: PropertyServices = new PropertyServices(req)
        const property = await services.update()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: property,
        })
    }

    destroy = async (req: Request, res: Response): Promise<Response> => {
        const services: PropertyServices = new PropertyServices(req)
        const property = await services.delete()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: property,
        })
    }

    verification = async (req: Request, res: Response): Promise<Response> => {
        const services: PropertyServices = new PropertyServices(req)
        const property = await services.verification()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: property,
        })
    }
}

export default new PropertyController()
