import { Request, Response } from 'express'

import IController from './interfaceControllers'
import ParticipantServices from '../services/participant.service'

class ParticipantController {
    index = async (req: Request, res: Response): Promise<Response> => {
        const services: ParticipantServices = new ParticipantServices(req)
        const participant = await services.findAll()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Data loaded',
            participant,
        })
    }

    store = async (req: Request, res: Response): Promise<Response> => {
        const services: ParticipantServices = new ParticipantServices(req)
        const participant = await services.store()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'New data created',
            participant,
        })
    }

    show = async (req: Request, res: Response): Promise<Response> => {
        const services: ParticipantServices = new ParticipantServices(req)
        const participant = await services.findbyId()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Data loaded',
            participant,
        })
    }

    showPropertyId = async (req: Request, res: Response): Promise<Response> => {
        const services: ParticipantServices = new ParticipantServices(req)
        const participant = await services.findbyProperty()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Data loaded',
            participant,
        })
    }

    update = async (req: Request, res: Response): Promise<Response> => {
        const services: ParticipantServices = new ParticipantServices(req)
        const participant = await services.update()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'New data updated',
        })
    }

    delete = async (req: Request, res: Response): Promise<Response> => {
        const services: ParticipantServices = new ParticipantServices(req)
        const participant = await services.delete()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'New data deleted',
        })
    }

    verification = async (req: Request, res: Response): Promise<Response> => {
        const services: ParticipantServices = new ParticipantServices(req)
        const participant = await services.verification()

        return res.send({
            status: res.statusCode,
            success: true,
            messages: 'Data verify',
        })
    }
}

export default new ParticipantController()
